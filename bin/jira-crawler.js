#!/usr/bin/env node

const yargs = require('yargs');
const crawlJIRA = require('../');

const argv = yargs
  .usage('Usage: $0 [options] <api_root> <issuetype..>')
  .command('* [options] <api_root> <issuetype..>')
  .example('$0 http://api.example.com type_1 type_2')
  .number(['b', 't', 'r'])
  .coerce({ 't': t => t * 1000 })
  .alias('b', 'batchSize')
  .nargs('b', 1)
  .default('b', 100)
  .describe('b', 'The number of concurrent requests for an issuetype')
  .alias('t', 'timeout')
  .nargs('t', 1)
  .default('t', 5)
  .describe('t', 'The timeout for requests in seconds')
  .alias('r', 'retries')
  .nargs('r', 1)
  .default('r', 6)
  .describe('r', 'The maximum number of retries on a failed request')
  .help()
  .argv;

const rootURL = argv._[0];
const issueTypes = argv._.slice(1);

crawlJIRA(rootURL, issueTypes, argv);
