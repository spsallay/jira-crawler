const express = require('express');
const { rchisq } = require('randgen');

const app = express();

const NUMBER_OF_ISSUES = 256;
const NUMBER_OF_TYPES = 5;
const MAX_SCORE = 13;
const FAILURE_RATE = 0.1;

function generateIssue(issueID) {
  const typeIndex = Math.floor(Math.random() * NUMBER_OF_TYPES) + 1;
  const estimate = Math.floor(Math.random() * MAX_SCORE) + 1;
  return {
    id: `/issues/${issueID}`,
    issuetype: `/issuetypes/type_${typeIndex}`,
    description: `Issue #${issueID}`,
    estimate
  }
}

function generateIssueType(issues, typeID) {
  const id = `/issuetypes/type_${typeID}`;
  const issuesOfType = issues.filter(issue => issue.issuetype === id);
  return {
    id,
    name: `type_${typeID}`,
    issues: issuesOfType.map(issue => issue.id),
    sum: issuesOfType.reduce((sum, issue) => sum + issue.estimate, 0)
  };
}

function generateData() {
  const issues = [];
  for (let i = 1; i <= NUMBER_OF_ISSUES; i++) {
    issues.push(generateIssue(i));
  }

  const types = [];
  for (let i = 1; i <= NUMBER_OF_TYPES; i++) {
    types.push(generateIssueType(issues, i));
  }

  return [types, issues];
}

const [types, issues] = generateData();

app.use((req, res, next) => {
  const willFail = Math.random() < FAILURE_RATE;

  if (willFail) {
    console.log(`Failed request: ${req.originalUrl}`);
    res.sendStatus(500);
  } else {
    next();
  }
});

app.use((req, res, next) => {
  const time = Math.floor(rchisq(2) * 1000)
  setTimeout(next, time);
});

app.get('/issuetypes/:typeID', (req, res) => {
  console.log('Accessing issuetype: ' + req.params.typeID);
  const issueType = types.find(type =>
    type.id === `/issuetypes/${req.params.typeID}`
  );

  if (issueType) {
    res.json(issueType);
  } else {
    res.sendStatus(404);
  }
});

app.get('/issues/:issueID', (req, res) => {
  console.log('Accessing issue: ' + req.params.issueID)
  const issue = issues.find(issue =>
    issue.id === `/issues/${req.params.issueID}`
  );

  if (issue) {
    res.json(issue);
  } else {
    res.sendStatus(404);
  }
});

app.listen(8080, () => {
  console.log('Server is listening on port 8080');
  types.forEach(type => {
    console.log(`${type.name}: ${type.sum}`);
  });
});
