JIRA Crawler
============

Installation
------------
Clone the repository and run `npm install`

Usage
-----

    Usage: bin/jira-crawler.js [options] <api_root> <issuetype..>

    Options:
      -b, --batchSize  The number of concurrent requests for an issuetype  [number] [default: 100]
      -t, --timeout    The timeout for requests in seconds                 [number] [default: 5 seconds]
      -r, --retries    The maximum number of retries on a failed requests  [number] [default: 6]
      --help           Show help                                           [boolean]

    Examples:
      bin/jira-crawler.js http://api.example.com type_1 type_2

Testing
-------
There is no formal test suite.

`node test/server.js` starts a small server at `localhost:8080` that offers a basic facsimile of the API. It produces a list of issuetypes `type_1, type_2, ...` and their corresponding issues for which to query.

The server runs slowly and fails randomly to simulate a chaotic, faulty environment.

Further Development
--------------------
* Ideally, I would rewrite this using either coroutines or async/await so that the asynchronous aspects of the code would be easier to understand at a glance. My experience in that area is lacking, however.
* Testing could use formal structure and actual unit/integration tests. Creating a dummy server seemed like a better use of the time I was asked to spend on this, as I could test assumptions I had regarding performance and fault tolerance.