const url = require('url');

const fetch = require('node-fetch');
const promiseRetry = require('promise-retry');
const { chunk } = require('lodash');

function tolerantFetch(url, options) {
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 400) {
      return response;
    } else {
      const error = new Error(response.statusText)
      error.response = response;
      throw error;
    }
  }

  const retryFetch = (retry, number) =>
    fetch(url, { timeout: options.timeout })
      .then(checkStatus)
      .then(res => res.json())
      .catch(retry);

  return promiseRetry(retryFetch, { retries: options.retries });
}

// function => Promise(A) => Promise(function(A))
function liftPromiseA1(action) {
  return promise => promise.then(action);
}

// issueType => Promise([issueID])
function fetchIssueType(rootURL, options, issueType) {
  return tolerantFetch(url.resolve(rootURL, `/issuetypes/${issueType}`), options)
    .then(response => response.issues)
    .catch(error => {
      console.error(`Failed to fetch issuetype "${issueType}": ${error.toString()}`)
      process.exit(1);
    });
}

// [[issueID]] => Promise(Number)
function fetchBatchesInSequence(rootURL, options, issueBatches) {
  const boundFetchIssues = fetchIssues.bind(null, rootURL, options);

  // [issueID] => Number => Promise(Number)
  const fetchBatchAndSum = batch => sum => boundFetchIssues(batch).then(sumIssueEstimates(sum));

  // (Promise(Number), [issueID]) => Promise(Number)
  const chainedFetchAndSum = (fetchedSum, batch) => fetchedSum.then(fetchBatchAndSum(batch));

  return issueBatches.reduce(chainedFetchAndSum, Promise.resolve(0));
}

// [issueID] => Promise([issue])
function fetchIssues(rootURL, options, issues) {
  const issueGroup = issues.map(issueID =>
    tolerantFetch(url.resolve(rootURL, issueID), options)
  );

  return Promise.all(issueGroup)
    .catch(error => {
      console.error(`Failed to fetch issue: ${error.toString()}`);
      process.exit(1);
    });
}

// Number => [issue] => Number
function sumIssueEstimates(baseSum = 0) {
  return issues => issues.reduce((sum, issue) => sum + issue.estimate, baseSum);
}

// [Number] => IO
function logEstimates(issueTypes, estimates) {
  estimates.forEach((estimate, i) => {
    console.log(`${issueTypes[i]}: ${estimate}`);
  });
}


function crawlJIRA(rootURL, issueTypes, options) {
  const batchIssues = issues => chunk(issues, options.batchSize);

  const boundFetchIssueType = fetchIssueType.bind(null, rootURL, options);
  const boundFetchBatches = fetchBatchesInSequence.bind(null, rootURL, options);
  const boundLogEstimates = logEstimates.bind(null, issueTypes);

  const liftedBatchIssues = liftPromiseA1(batchIssues);
  const liftedFetchBatches = liftPromiseA1(boundFetchBatches);
  const liftedConsoleLog = liftPromiseA1(c => { console.log(c); return c; });

  const estimates = issueTypes  // [issueType]
    .map(boundFetchIssueType)   // => [Promise([issueID])]
    .map(liftedBatchIssues)     // => [Promise([[issueID]])]
    .map(liftedFetchBatches)    // => [Promise(Number)]

  Promise.all(estimates)
    .then(boundLogEstimates);
}

module.exports = crawlJIRA;